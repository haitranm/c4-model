workspace {

    model {
        // Level 1: System Context
        # person
        publicUser = person "Public User" "Public User via REST API." "User"
        authorizedUser = person "Authorized User" "Authorized User via REST API." "User"

        # softwaresystem
        booksStoreSystem = softwaresystem "Books Store System" "Allows users to interact with book records." {
            // Level 2: Container
            searchWebAPI = container "Search Web API" "Allows searching books records." "Go"
            adminWebAPI = container "Admin Web API" "Allows administering books details." "Go"
            publicWebAPI = container "Public Web API" "Allows Public users getting books details."

            elasticSearchEventsConsumer = container "ElasticSearch Events Consumer" "Updates details coming from domain events." "Go"

            searchDB = container "Search Database" "Stores searchable books details." "ElasticSearch" "Database"
            rwRelationalDB = container "Read/Write Relational Database" "Stores books details." "PostgreSQL" "Database"
            readersCache = container "Readers Cache" "Caches books details." "Memcached" "Database"
            // TBC: maybe enhancement here for Read-only DB
            # rOnlyRelationalDB = container "Read-Only Relational Database" "Replicas for books details." "PostgreSQL" "Database"

            publisherRecurrentUpdater = container "Publisher Recurrent Updater" "Updates the Read/Write Relational Database with detail from Publisher system." "Go"
        }

        bookKafkaSystem = softwaresystem "Book Kafka System, Apache Kafka 3.0" "Handles book-related domain events." "External System"
        authSystem = softwaresystem "Authorization System" "Authorizes access to resources." "External System"
        publisherSystem = softwaresystem "Publisher System" "Gives details about books published by them." "External System"

        # relationships between person and software systems
        authorizedUser -> adminWebAPI "Makes API calls" "JSON/HTTPS"
        authorizedUser -> searchWebAPI "Makes API calls" "JSON/HTTPS"

        # relationships to/from containers
        // TBC: rwRelationalDB -> rOnlyRelationalDB "Writes data to" "SQL"
        searchWebAPI -> searchDB "Reads data from" "ElasticSearch"
        searchWebAPI -> authSystem "Authorizes access for records" "HTTPS"

        adminWebAPI -> bookKafkaSystem "Publishes events to" "Kafka"
        adminWebAPI -> authSystem "Authorizes access for records" "HTTPS"
        adminWebAPI -> rwRelationalDB "Reads from and writes data to" "SQL"

        // TBC: publicWebAPI -> rOnlyRelationalDB "Reads data from" "SQL"
        publicWebAPI -> rwRelationalDB "Reads data from" "SQL"
        publicWebAPI -> readersCache "Reads/Writes data using" "Memcached"
        publicUser -> publicWebAPI "Makes API calls" "JSON/HTTPS"

        elasticSearchEventsConsumer -> bookKafkaSystem "Listens domain events from" "Kafka"
        elasticSearchEventsConsumer -> searchDB "Writes publisher to" "ElasticSearch"

        publisherRecurrentUpdater -> publisherSystem "Listens to external events coming" "Kafka"
        publisherRecurrentUpdater -> adminWebAPI "Writes Publisher details using" "HTTPS"
    }

    views {
        # Level 1: System Context view
        systemContext booksStoreSystem "SystemContext" {
            include *
            autoLayout
        }

        # Level 2: Container view
        container booksStoreSystem "Containers" {
            include *
            autoLayout
        }

        styles {
            element "User" {
                shape Person
                background #7b085e
                color #ffffff
                fontsize 22
            }
            element "Software System" {
                background #1168bd
                color #ffffff
            }
            element "Container" {
                background #438dd5
                color #ffffff
            }
            element "External System" {
                background #999999
                color #ffffff
            }
            element "Database" {
                shape Cylinder
            }
        }
    }
    
}
