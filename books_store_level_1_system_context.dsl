workspace {

    model {
        # Level 1: System Context
        # person
        publicUser = person "Public User" "A user of Books Store." "User"
        authorizedUser = person "Authorized User" "A user of Books Store." "User"

        # softwaresystem
        booksStoreSystem = softwaresystem "Books Store System" "Allows users to interact with book records."
        authSystem = softwaresystem "Authorization System" "Authorizes access to resources." "External System"
        publisherSystem = softwaresystem "Publisher System" "Gives details about books published by them." "External System"

        # relationships between person and software systems
        publicUser -> booksStoreSystem "Get books details"
        authorizedUser -> booksStoreSystem "Get books details, Search books records and Administer books details"
        booksStoreSystem -> authSystem "Get User's information"
        booksStoreSystem -> publisherSystem "Get Book's information"
    }

    views {
        # Level 1: System Context view
        systemContext booksStoreSystem "SystemContext" {
            include *
            autoLayout
        }

        styles {
            element "User" {
                shape Person
                background #7b085e
                color #ffffff
                fontsize 22
            }
            element "Software System" {
                background #1168bd
                color #ffffff
            }
            element "External System" {
                background #999999
                color #ffffff
            }
        }
    }
    
}
