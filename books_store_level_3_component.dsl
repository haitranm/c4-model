workspace {

    model {
        // Level 1: System Context
        # person
        authorizedUser = person "Authorized User" "Authorized User via REST API." "User"

        # softwaresystem
        booksStoreSystem = softwaresystem "Books Store System" "Allows users to interact with book records." {
            // Level 2: Container
            searchWebAPI = container "Search Web API" "Allows searching books records." "Go"
            publicWebAPI = container "Public Web API" "Allows Public users getting books details."
            adminWebAPI = container "Admin Web API" "Allows administering books details." "Go" {
                // Level 3: Component
                bookService = component "Book" "Allows administering books details." "service.Book"
                authService = component "Authorization" "Allows authorizing users." "service.Authorizer"
                eventsPublisherService = component "EventsPublisher" "Publishes books-related domain events." "service.EventsPublisher"
            }

            publisherRecurrentUpdater = container "Publisher Recurrent Updater" "Updates the Read/Write Relational Database with detail from Publisher system." "Go"

            relationalDB = container "Relational Database" "Stores books details." "PostgreSQL" "Database"
        }

        authSystem = softwaresystem "Authorization System" "Authorizes access to resources." "External System"
        bookKafkaSystem = softwaresystem "Book Kafka System, Apache Kafka 3.0" "Handles book-related domain events." "External System"

        # relationships to/from containers
        publisherRecurrentUpdater -> bookService "Writes Publisher details using" "HTTPS"

        # relationships to/from components
        authorizedUser -> bookService "Administers books details" "JSON/HTTPS"
        bookService -> authService "Authorizes books details" "JSON/HTTPS"
        bookService -> eventsPublisherService "Publishes books-related events"
        bookService -> relationalDB "Reads from and writes data to" "SQL"

        eventsPublisherService -> bookKafkaSystem "Publishes books-related events" "Kafka"

        authService -> authSystem "Uses" "HTTPS"
    }

    views {
        # Level 1: System Context view
        systemContext booksStoreSystem "SystemContext" {
            include *
            autoLayout
        }

        # Level 2: Container view
        container booksStoreSystem "Containers" {
            include *
            autoLayout
        }

        # Level 3: Component view
        component adminWebAPI "Components" {
            include *
            autoLayout
        }

        styles {
            element "User" {
                shape Person
                background #7b085e
                color #ffffff
                fontsize 22
            }
            element "Software System" {
                background #1168bd
                color #ffffff
            }
            element "Container" {
                background #438dd5
                color #ffffff
            }
            element "External System" {
                background #999999
                color #ffffff
            }
            element "Database" {
                shape Cylinder
            }
            element "Component" {
                background #85bbf0
                color #000000
            }
        }
    }
    
}
